<?php

namespace Finpaa\Exceptions;

use Exception;
use Illuminate\Http\Response;

class FinpaaException extends Exception
{
    public function render($request): Response
    {
        return response([
            'error' => true,
            'title' => 'Finpaa Exception',
            'message' => $this->getMessage()
        ], 400);
    }
}