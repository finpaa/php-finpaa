<?php

namespace Finpaa\Providers;

use Finpaa\Exceptions\FinpaaException;

class EndPointProvider
{
    const _GET_COUNTRY = 'country';
    const _GET_ENVIRONMENT_TYPE = 'environmentType';
    const _GET_COUNTRY_PROVIDER = 'countryProviders/[[_CODE_]]';
    const _GET_PROVIDER_CATEGORY = 'providerCategory';
    const _GET_CATEGORY_PROVIDER = 'categoryProviders/[[_CODE_]]';
    const _GET_PROVIDER_SEQUENCE = 'providerSequences/[[_CODE_]]';
    const _GET_PROVIDER_PRODUCT = 'providerProducts/[[_CODE_]]';

    const _GET_PROVIDER_PRODUCT_METHOD = 'providerProductMethod/[[_PRODUCT_CODE_]]/[[_ENV_CODE_]]';
    const _GET_SEQUENCE_METHOD = 'sequenceMethods/[[_CODE_]]';
    const _GET_EXECUTABLE_METHOD = 'providerProductMethodToExecute/[[_CODE_]]';
    const _GET_EXECUTE_METHOD = 'execute/[[_ENV_TYPE_]][[_FINPAA_ROUTE_]]';

    public static function route(string $routeName, array $arguments = []): string
    {
        $path = 'self::'. $routeName;

        if(!defined($path)) throw new FinpaaException("Undefined route " . $routeName);

        $path = constant($path);

        foreach ($arguments as $key => $value)
        {
            $path = str_replace("[[" . $key . "]]", $value, $path);
        }

        return $path; 
    }
}