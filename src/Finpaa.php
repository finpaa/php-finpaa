<?php

namespace Finpaa;

use Exception;
use Finpaa\Exceptions\FinpaaException;
use Finpaa\Providers\EndPointProvider;

class Finpaa
{
    const FINPAA_ENDPOINT = 'https://app.finpaa.com/';
    const ROUTE_VARIABLE_REGEX = '/\[\[(.+?)\]\]/m';
    const SERVER_DISCONNECTIVITY_ERROR = "Couldn't connect to server";

    private static $ACCESS_TOKEN;
    private static $FINPAA_ENDPOINT;

    function __construct()
    {
        self::$FINPAA_ENDPOINT = env('FINPAA_ENDPOINT', self::FINPAA_ENDPOINT);
        $this->processCountries();
    }
    public function __call($closure, $args)
    {
        try {
            return call_user_func_array($this->{$closure}->bindTo($this), $args);
        } catch (Exception $e) {
            throw new FinpaaException($e->getMessage());
        }
    }

    private static function getAccessToken()
    {
        if(self::$ACCESS_TOKEN) {
            return self::$ACCESS_TOKEN;
        }
        else {

            $body = array(
                'clientId' => env('FINPAA_CLIENT_ID'),
                'clientSecret' => env('FINPAA_CLIENT_SECRET'),
            );

            $curl = curl_init();
            $options = array(
                CURLOPT_URL => self::$FINPAA_ENDPOINT . 'oauth/login',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($body),
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Content-Type: application/json",
                )
            );

            curl_setopt_array($curl, $options);
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return "cURL Error #:" . $err;
            }

            $response = json_decode($response, true);

            if(isset($response['accessToken']))
            {
                self::$ACCESS_TOKEN = $response['tokenType'] . ' ' . $response['accessToken'];
            }
            else
            {
                self::$ACCESS_TOKEN = 'Unauthorized';
            }
            return self::getAccessToken();
        }
    }

    private static function getClass()
    {
        return new class
        {
            public function __call($closure, $args)
            {
                try {
                    return call_user_func_array($this->{$closure}->bindTo($this), $args);
                } catch (Exception $e) {
                    throw new FinpaaException($e->getMessage());
                }
            }
        };
    }
    public static function camelCase($str, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = preg_replace('/\ba\b/i', ' ', $str);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }

    private static function fileAsString($path) {
        $fileContent = file_get_contents($path);
        $fileContent = preg_replace('/\n/', '\\n', $fileContent);
        $fileContent = preg_replace('/\r/', '\\r', $fileContent);
      return $fileContent; //preg_replace('/\n/', '\\n', file_get_contents($path));
    }
    public static function getCerts($certPath, $keyPath) {
        return array(
            'cert' => self::fileAsString($certPath),
            'key' => self::fileAsString($keyPath)
        ); 
    }
 
    private function processCountries()
    {
        $countries = self::getCountries();

        if(!(isset($countries->error) && $countries->error) || !isset($countries->message))
        {
            foreach ($countries as $country) {
                $functionName = $country->name;
                $this->$functionName = function () use ($country) {
                    return self::processEnvironmentTypes($country->code);
                };
            }
            return $this;
        }
        else {
            throw new FinpaaException($countries->message);
        }
    }
    private static function processEnvironmentTypes($countryCode)
    {
        $environments = self::getEnvironmentTypes();
        $node =     self::getClass();
        foreach ($environments as $env) {
            $functionName = $env->name;
            $node->$functionName = function () use ($countryCode, $env) {
                return self::processCountryProviders($countryCode, $env->code);
            };
        };
        return $node;
    }
    private static function processCountryProviders($code, $environmentCode = null)
    {
        $providers = self::getCountryProviders($code, $environmentCode);
        $node = self::getClass();
        foreach ($providers as $provider) {
            $functionName = $provider->name;
            $node->$functionName = function () use ($provider, $environmentCode) {
                return self::processProviderSequences($provider->code, $environmentCode);
            };
        }
        return $node;
    }
    private static function processProviderSequences($code, $environmentCode)
    {
        $sequences = self::getProviderSequences($code);
        $node = self::getClass();

        $functionName = 'Products';
        $node->$functionName = function () use ($code, $environmentCode) {
            return self::processProviderProducts($code, $environmentCode);
        };

        foreach ($sequences as $sequence) {
            $functionName = $sequence->name;
            $node->$functionName = function () use ($sequence) {
                return $sequence->code;
            };
        };
        return $node;
    }
    private static function processProviderProducts($code, $environmentCode)
    {
        $products = Finpaa::getProviderProducts($code);
        $node = self::getClass();

        foreach ($products as $product) {
            $functionName = self::camelCase($product->name);
            $node->$functionName = function () use ($product, $environmentCode) {
                return self::processProductMethods($product->code, $environmentCode);
            };
        };
        return $node;
    }
    private static function processProductMethods($code, $environmentCode)
    {
        $methods = Finpaa::getProductMethods($code, $environmentCode);
        $node = self::getClass();
        foreach ($methods as $method) {
            $functionName = self::camelCase($method->name);
            $node->$functionName = function () use ($method) {
                return $method->code;
            };
        };
        return $node;
    }

    private static function callFinpaa(string $path, $method = 'GET', $headers = [], $body = null): mixed
    {
        $curl = curl_init();
        $options = array(
            CURLOPT_URL => self::$FINPAA_ENDPOINT . $path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $body ? json_encode($body) : '',
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Finpaa-Auth: " . env('FINPAA_ACCESS_TOKEN')
            )
        );

        if (count($headers) > 0) {
            foreach ($headers as $key => $value) {
                $options[CURLOPT_HTTPHEADER][] = $key . ": " . $value;
            }
        }

        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {

            $message = $err;

            if(str_contains($err, self::SERVER_DISCONNECTIVITY_ERROR))
                $message = 'System is unable to connect with Finpaa Server';

            throw new FinpaaException($message);
        }

        return json_decode($response);
    }
    static function getCountries($code = null)
    {
        $path = EndPointProvider::route('_GET_COUNTRY');

        if ($code) $path .= '/' . $code;

        return self::callFinpaa($path);
    }
    static function getEnvironmentTypes($code = null)
    {
        $path = EndPointProvider::route('_GET_ENVIRONMENT_TYPE');

        if ($code) $path .= '/' . $code;

        return self::callFinpaa($path);
    }
    static function getCountryProviders($code, $environmentCode = null)
    {
        $path = EndPointProvider::route('_GET_COUNTRY_PROVIDER', ['_CODE_' => $code]);

        if ($environmentCode) $path .= '?environmentTypeCode=' . $environmentCode;

        return self::callFinpaa($path);
    }
    static function getProviderCategories($code = null)
    {
        $path = EndPointProvider::route('_GET_PROVIDER_CATEGORY');

        if ($code) $path .= '/' . $code;

        return self::callFinpaa($path);
    }
    static function getCategoryProviders($code)
    {
        $path = EndPointProvider::route('_GET_CATEGORY_PROVIDER', ['_CODE_' => $code]);
        return self::callFinpaa($path);
    }
    static function getProviderSequences($code)
    {
        $path = EndPointProvider::route('_GET_PROVIDER_SEQUENCE', ['_CODE_' => $code]);
        return self::callFinpaa($path);
    }
    static function getProviderProducts($code)
    {
        $path = EndPointProvider::route('_GET_PROVIDER_PRODUCT', ['_CODE_' => $code]);
        return self::callFinpaa($path);
    }
    static function getProductMethods($productCode, $envCode)
    {
        $path = EndPointProvider::route('_GET_PROVIDER_PRODUCT_METHOD', [
            '_PRODUCT_CODE_' => $productCode,
            '_ENV_CODE_' => $envCode
        ]);

        return self::callFinpaa($path);
    }
    static function getSequenceMethods($sequenceCode)
    {
        $path = EndPointProvider::route('_GET_SEQUENCE_METHOD', ['_CODE_' => $sequenceCode]);
        return self::callFinpaa($path);
    }
    static function getMethodToExecute($code)
    {
        $path = EndPointProvider::route('_GET_EXECUTABLE_METHOD', ['_CODE_' => $code]);

        $response = self::callFinpaa($path);
        if (isset($response->ProviderApiProducts)) {
            $modifiedResponse = array();
            $modifiedResponse["fields"] = $response->ProviderApiProducts[0]->ProviderProductMethods[0]->Fields;
            if (count($modifiedResponse["fields"]) > 0) {
                $MFs = array();
                foreach ($modifiedResponse["fields"] as $item) {
                    $array = array();
                    $array['code'] = $item->code;
                    $array['label'] = $item->title;
                    $array['name'] = $item->key;
                    $array['type'] = $item->FieldType->name;
                    $array['comment'] = $item->comment;
                    $array['category'] = $item->MethodRequestOption->name;
                    $array['childOf'] = $item->ChildOf->code ?? null;
                    $array['options'] = array_map(function ($return) {
                        return $return->value;
                    }, $item->FieldPossibleValues);
                    $MFs[] = $array;
                }
                $modifiedResponse["fields"] = $MFs;
            }
            unset($response->ProviderApiProducts[0]->ProviderProductMethods[0]->Fields);
            unset($response->ProviderApiProducts[0]->ProviderProductMethods[0]->baseUrl);
            // unset($response->ProviderApiProducts[0]->ProviderProductMethods[0]->route);
            $method = $response->ProviderApiProducts[0]->ProviderProductMethods[0];
            $methodModified['environmentAvailability'] = $method->EnviornmentAvailability->name;
            $methodModified['environmentType'] = $method->EnviornmentType->name;
            $methodModified['methodType'] = $method->MethodType->name;
            unset($method->EnviornmentAvailability);
            unset($method->EnviornmentType);
            unset($method->MethodType);
            $modifiedResponse["providerProductMethod"] = array_merge((array)$method, $methodModified);
            unset($response->ProviderApiProducts[0]->ProviderProductMethods);
            $modifiedResponse['providerApiProduct'] = $response->ProviderApiProducts[0];
            unset($response->ProviderApiProducts);
            unset($response->Country);
            unset($response->protocol);
            unset($response->base_url);
            $response = array_merge((array)$response, $modifiedResponse);
        }
        return $response;
    }
    static function executeMethod($path, $methodType, $payload)
    {
        if (isset($payload['path']) && count($payload['path']) > 0) {
            $path .= '/' . join("/", $payload['path']);
        }
        if (isset($payload['query'])) {
            $path .= '?' . http_build_query($payload['query']);
        }
        return self::callFinpaa($path, $methodType, $payload['headers'] ?? [], $payload['body'] ?? null);
    }
    // These functions are required for each integration, so placing them here
    private static function findParent($fields, $parentCode)
    {
        $parent = array_search($parentCode, array_column($fields, 'code'));
        return $fields[$parent];
    }
    private static function addParent($fields, $field, $value = array()): array
    {
        $currentValue = array();
        if ($value) {
            $currentValue[$field['name']] = $value;
        } else {
            $currentValue[$field['name']] = $field['options'][0] ?? null;
        }
        $value = $currentValue;
        $parent = null;
        if ($field['childOf']) {
            $parent = self::findParent($fields, $field['childOf']);
        }
        if ($parent) {
            $value = self::addParent($fields, $parent, $value);
        }
        return $value;
    }
    static function executeTheMethod($code, $alterations = [], $returnPayload = false)
    {
        $method = self::getMethodToExecute($code);
        if ($method && isset($method['providerProductMethod'])) {

            $path = EndPointProvider::route('_GET_EXECUTE_METHOD', [
                '_ENV_TYPE_' => $method['providerProductMethod']['environmentType'],
                '_FINPAA_ROUTE_' => $method['providerProductMethod']['finpaaRoute']
            ]);

            $payload = array();
            foreach ($method['fields'] as $field) {
                $value = self::addParent($method['fields'], $field);
                $payload[$field['category']] = array_replace_recursive($payload[$field['category']] ?? [], $value);
            }
            // return $payload;
            if (count($alterations) > 0) {
                foreach ($alterations as $item) {
                    $payload = array_replace_recursive($payload ?? [], $item);
                }
            }
            // return $payload;

            if (isset($payload['path']) && count($payload['path']) > 0) {
                preg_match_all(
                    self::ROUTE_VARIABLE_REGEX,
                    $method['providerProductMethod']['route'],
                    $matches, PREG_SET_ORDER, 0
                );

                $sequencedPath = [];
                foreach($matches as $item)
                {
                    $sequencedPath[$item[1]] = $payload['path'][$item[1]];
                }
                $payload['path'] = $sequencedPath;
            }

            $response = self::executeMethod($path, $method['providerProductMethod']['methodType'], $payload);
            if ($returnPayload)
                return array('payload' => $payload, 'response' => $response);
            else return $response;
        } else return $method;
    }
}
